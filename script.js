plaza1 = document.getElementById('plaza1')
plaza2 = document.getElementById('plaza2')
plaza3 = document.getElementById('plaza3')
plaza4 = document.getElementById('plaza4')
plaza5 = document.getElementById('plaza5')
plaza6 = document.getElementById('plaza6')
plaza7 = document.getElementById('plaza7')
plaza8 = document.getElementById('plaza8')
plaza9 = document.getElementById('plaza9')
plaza10 = document.getElementById('plaza10')
plaza11 = document.getElementById('plaza11')
plaza12 = document.getElementById('plaza12')
plaza13 = document.getElementById('plaza13')
plaza14 = document.getElementById('plaza14')
plaza15 = document.getElementById('plaza15')
plaza16 = document.getElementById('plaza16')
plaza17 = document.getElementById('plaza17')
plaza18 = document.getElementById('plaza18')

arrastrarElemento(plaza1);
arrastrarElemento(plaza2);
arrastrarElemento(plaza3);
arrastrarElemento(plaza4);
arrastrarElemento(plaza5);
arrastrarElemento(plaza6);
arrastrarElemento(plaza7);
arrastrarElemento(plaza8);
arrastrarElemento(plaza9);
arrastrarElemento(plaza10);
arrastrarElemento(plaza11);
arrastrarElemento(plaza12);
arrastrarElemento(plaza13);
arrastrarElemento(plaza14);
arrastrarElemento(plaza15);
arrastrarElemento(plaza16);
arrastrarElemento(plaza17);
arrastrarElemento(plaza18);

colocarPuntero(plaza1);
colocarPuntero(plaza2);
colocarPuntero(plaza3);
colocarPuntero(plaza4);
colocarPuntero(plaza5);
colocarPuntero(plaza6);
colocarPuntero(plaza7);
colocarPuntero(plaza8);
colocarPuntero(plaza9);
colocarPuntero(plaza10);
colocarPuntero(plaza11);
colocarPuntero(plaza12);
colocarPuntero(plaza13);
colocarPuntero(plaza14);
colocarPuntero(plaza15);
colocarPuntero(plaza16);
colocarPuntero(plaza17);
colocarPuntero(plaza18);


function arrastrarElemento(elementoDePlaza) {
  // establecer 4 posiciones para posicionar en la pantalla
  let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  // cuando el evento onpointerdown se activa llamamos a nuestra funcion arrastrarPuntero
  elementoDePlaza.onpointerdown = arrastrarPuntero;

  function arrastrarPuntero(e) {
    /* 
    * cancela cualquier acción por defecto que deba ocurrir luego
    * de invocar al evento onpointerdown, en este caso.
    */
    e.preventDefault();
    console.log(e);
    // posiciones del puntero
    pos3 = e.clientX;
    pos4 = e.clientY;
    console.log(pos1,pos2,pos3,pos4)
    // evento de movimiento de puntero
    document.onpointermove = arrastrarElemento;
    // evento de parada de puntero
    document.onpointerup = detenerArrastreElemento;

    function arrastrarElemento(e) {
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      console.log(pos1, pos2, pos3, pos4);
      elementoDePlaza.style.top = elementoDePlaza.offsetTop - pos2 + 'px';
      elementoDePlaza.style.left = elementoDePlaza.offsetLeft - pos1 + 'px';
    }

    function detenerArrastreElemento() {
      document.onpointerup = null;
      document.onpointermove = null;
    }

  }

}

function colocarPuntero(elementoDePlaza) {
  // definicion de evento de ebtrada de mouse
  elementoDePlaza.addEventListener("mouseenter", (event) => {
    // con esto le damos un nuevo estilo a la imagen al pasar el mouse por encima
    event.target.style.border = '2px solid black';
  }, false);

  // definicion de evento de salida de mouse
  elementoDePlaza.addEventListener("mouseleave", (event) => {
    // con esto reseteamos al valor original el borde al sacar el mouse de encima de la imagen
    event.target.style.border = "";
  }, false);
}